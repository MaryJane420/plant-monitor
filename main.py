# -*- coding: utf-8 -*-
import time
from PMTemp import PMTemp
from PMCamera import PMCamera
from PMMessage import PMMessage

#VARIABLES
pollingInterval = 600  #In seconds
TEMP_High = 85
TEMP_Low = 70
SMS_Notifications = False
pmMessage = PMMessage(SMS_Notifications)
pmTemp = PMTemp(pmMessage, TEMP_High, TEMP_Low)
pmCamera = PMCamera()
log = open('/home/pi/Desktop/Python Programming/plant-monitor/logs/PMLog.txt', 'a')


#FUNCTIONS


#LOOP
while True:
    print("-------------------------------------")
    print(time.strftime("%m/%d/%Y %I:%M:%S %p") + " - Plant Monitor Started")
    log.write("\n-------------------------------------\n" + time.strftime("%m/%d/%Y %I:%M:%S %p") + " - Plant Monitor Started")
    log.flush()
    pmTemp.getTemp()
    pmCamera.takePic()
    print(time.strftime("%m/%d/%Y %I:%M:%S %p") + " - Plant Monitor Completed")
    log.write("\n-------------------------------------\n" + time.strftime("%m/%d/%Y %I:%M:%S %p") + " - Plant Monitor Completed")
    log.flush()
    time.sleep(pollingInterval)